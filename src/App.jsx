import { useState } from "react";
import {
  RiMenu3Fill,
  RiUserLine,
  RiAddFill,
  RiPieChart2Fill,
  RiCloseFill,
  RiArrowDownSLine,
} from "react-icons/ri";

// Components
import Sidebar from "./components/shared/Sidebar";
import Header from "./components/shared/Header";
import Cart from "./components/shared/Cart";
import Card from "./components/shared/Card";

// Components
function App() {
  const [showMenu, setShowMenu] = useState(false);
  const [showOrder, setShowOrder] = useState(false);

  const toggleMenu = () => {
    setShowMenu(!showMenu);
    setShowOrder(false);
  };

  const toggleOrder = () => {
    setShowOrder(!showOrder);
    setShowMenu(false);
  };

  return (
    <>
      <div className="bg-[#262837] w-full min-h-screen">
        <Sidebar showMenu={showMenu} />
        <Cart showOrder={showOrder} setShowOrder={setShowOrder} />
        {/* Menú móvil */}
        <nav className="bg-[#1f1d2b] lg:hidden fixed w-full bottom-0 left-0 text-3xl text-gray-400 py-2 px-8 flex items-center justify-between rounded-tl-xl rounded-tr-xl">
          <button className="p-2">
            <RiUserLine />
          </button>
          <button className="p-2">
            <RiAddFill />
          </button>
          <button onClick={toggleOrder} className="p-2">
            <RiPieChart2Fill />
          </button>
          <button onClick={toggleMenu} className="text-white p-2">
            {showMenu ? <RiCloseFill /> : <RiMenu3Fill />}
          </button>
        </nav>

        <main className="lg:pl-32 lg:pr-96 pb-20">
          <div className="md:p-16 p-4">
            {/* Header */}
            <Header />

            {/* Title content*/}
            <div className="flex items-center justify-between mb-16">
              <h2 className="text-xl text-gray-300">Choose Dishes</h2>
              <button className="flex items-center gap-4 text-gray-300 bg-[#1f1d2b] py-2 px-4 rounded-lg">
                <RiArrowDownSLine />
                Dine in
              </button>
            </div>
            {/* Content */}
            <div className="p-8 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-20">
              {/* Card */}
              <Card
                img="/img/image1.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
              {/* Card */}
              <Card
                img="/img/image5.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
              {/* Card */}
              <Card
                img="/img/image5.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
              {/* Card */}
              <Card
                img="/img/image1.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
              {/* Card */}
              <Card
                img="/img/image5.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
              {/* Card */}
              <Card
                img="/img/image5.png"
                description="Speacy seasoned seafood nodles"
                price="2.29"
                inventory="20 Bowls available"
              />
            </div>
          </div>
        </main>
      </div>
    </>
  );
}

export default App;
