const Card = (props) => {
  // eslint-disable-next-line react/prop-types
  const { img, description, price, inventory } = props;
  return (
    <div className="bg-[#1f1d2b] p-8 rounded-xl flex flex-col items-center gap-2 text-center text-gray-300">
      <img
        src={img}
        className="w-50 hv-50 object-cover -mt-20 shadow-xl rounded-full"
        alt=""
      />
      <p className="text-xl">{description}</p>
      <span className="text-gray-400">${price}</span>
      <p className="text-gray-600">{inventory}</p>
    </div>
  );
};

export default Card;
